import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./components/layout/home/home.component";
import { CatalogueComponent } from "./components/layout/catalogue/catalogue.component";
import { AboutComponent } from "./components/layout/about/about.component";

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "order", component: CatalogueComponent },
  { path: "about", component: AboutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
