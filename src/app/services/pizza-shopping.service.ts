import { Injectable, Inject } from "@angular/core";
import { v4 as uuid } from "uuid";
import { Observable, of, merge, Subject, BehaviorSubject } from "rxjs";
import {
  scan,
  startWith,
  map,
  tap,
  combineLatest,
  switchMap,
  skipWhile,
  shareReplay,
  debounceTime,
  publish,
  refCount,
  share,
} from "rxjs/operators";
import { StateTree, CartItem, Totals } from "../models/data-interfaces";

import { PizzaService } from "./pizza.service";

@Injectable({
  providedIn: "root",
})
export class PizzaShoppingService {
  constructor(private pizzaService: PizzaService) {}

  private stateTree$ = new BehaviorSubject<StateTree>(null);
  private checkoutTrigger$ = new BehaviorSubject<boolean>(false);
  private cartAdd$ = new Subject<CartItem>();
  private cartRemove$ = new Subject<CartItem>();

  private get cart$(): Observable<CartItem[]> {
    return merge(this.cartAdd$, this.cartRemove$).pipe(
      startWith([]),
      scan((acc: CartItem[], item: CartItem) => {
        if (item) {
          if (item.remove) {
            return [...acc.filter((i) => i.uuid !== item.uuid)];
          }
          return [...acc, item];
        }
      })
    );
  }

  private get total$(): Observable<Totals> {
    return this.cart$.pipe(
      map((items) => {
        let total = 0;
        for (const i of items) {
          total += i.price;
        }
        return total;
      }),
      map((cost) => ({
        subTotal: cost,
        delivery: 0.017 * cost,
        grandTotal: 0.017 * cost + cost,
      }))
    );
  }

  state$: Observable<StateTree> = this.stateTree$.pipe(
    switchMap(() =>
      this.getPizza().pipe(
        combineLatest([this.cart$, this.total$, this.checkoutTrigger$]),
        debounceTime(0)
      )
    ),
    map(([store, cart, total, checkout]: any) => ({
      store,
      cart,
      total,
      checkout,
    })),
    tap((state) => {
      if (state.checkout) {
        console.log("checkout", state);
      }
    }),
    shareReplay(1)
  );

  private getPizza(): Observable<any> {
    return of(this.pizzaService.getAllPizzas());
  }

  addCartItem(item: CartItem) {
    this.cartAdd$.next({ ...item, uuid: uuid() });
  }

  removeCartItem(item: CartItem) {
    this.cartRemove$.next({ ...item, remove: true });
  }

  checkout() {
    this.checkoutTrigger$.next(true);
  }
}
