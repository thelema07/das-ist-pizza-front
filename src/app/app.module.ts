import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SharedModule } from "./components/shared/shared.module";
import { BootstrapModule } from "./components/bootstrap/bootstrap.module";
import { LayoutModule } from "./components/layout/layout.module";
import { RouterModule } from "@angular/router";
import { CarouselModule } from "ngx-bootstrap/carousel";

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    SharedModule,
    LayoutModule,
    BootstrapModule,
    CarouselModule.forRoot(),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
