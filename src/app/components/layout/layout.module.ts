import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CatalogueComponent } from "./catalogue/catalogue.component";
import { AboutComponent } from "./about/about.component";
import { HomeComponent } from "./home/home.component";
import { BootstrapModule } from "../bootstrap/bootstrap.module";
import { CarouselModule } from "ngx-bootstrap/carousel";
import { ModalModule } from "ngx-bootstrap/modal";
import { PizzaService } from "src/app/services/pizza.service";
import { PizzaOrderComponent } from "./catalogue/pizza-order/pizza-order.component";

@NgModule({
  declarations: [
    CatalogueComponent,
    AboutComponent,
    HomeComponent,
    PizzaOrderComponent,
  ],
  imports: [CommonModule, CarouselModule.forRoot(), ModalModule.forRoot()],
  exports: [CatalogueComponent, AboutComponent, HomeComponent],
  providers: [PizzaService],
})
export class LayoutModule {}
