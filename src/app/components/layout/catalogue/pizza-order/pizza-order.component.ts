import { Component, OnInit, TemplateRef } from "@angular/core";
import { PizzaShoppingService } from "src/app/services/pizza-shopping.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

@Component({
  selector: "app-pizza-order",
  templateUrl: "./pizza-order.component.html",
  styleUrls: ["./pizza-order.component.scss"],
})
export class PizzaOrderComponent implements OnInit {
  cartState$ = this.pizzaShoppingService.state$;
  modalRef: BsModalRef;

  constructor(
    private pizzaShoppingService: PizzaShoppingService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {}

  orderCheckout(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.pizzaShoppingService.checkout();
  }

  removePizza(pizza) {
    this.pizzaShoppingService.removeCartItem(pizza);
  }
}
