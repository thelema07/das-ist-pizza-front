import { Component, OnInit, OnDestroy, HostListener } from "@angular/core";
import { PizzaService } from "src/app/services/pizza.service";
import { Subscription, Observable } from "rxjs";
import { CartItem } from "src/app/models/data-interfaces";
import { PizzaShoppingService } from "src/app/services/pizza-shopping.service";

@Component({
  selector: "app-catalogue",
  templateUrl: "./catalogue.component.html",
  styleUrls: ["./catalogue.component.scss"],
})
export class CatalogueComponent implements OnInit, OnDestroy {
  private subscription = new Subscription();
  pizzaCatalogue: Observable<CartItem[]>;
  showCheckoutWindow: boolean = false;
  showResponsive: boolean = false;
  cartState$ = this.pizzaShoppingService.state$;
  @HostListener("window:scroll", ["$event"]) // for window scroll events
  onScroll(event) {
    if (event.target.documentElement.scrollTop > 1610) {
      this.showCheckoutWindow = false;
    } else if (
      event.target.documentElement.innerWidth < 700 &&
      event.target.documentElement.scrollTop > 3000
    ) {
      this.showCheckoutWindow = false;
    } else {
      this.showCheckoutWindow = true;
    }
  }

  constructor(
    private pizzaService: PizzaService,
    private pizzaShoppingService: PizzaShoppingService
  ) {}

  ngOnInit() {
    const pizzaSubscription = this.pizzaService
      .getAllPizzas()
      .subscribe((response) => {
        this.pizzaCatalogue = response;
      });

    this.subscription.add(pizzaSubscription);

    if (window.innerWidth > 700) {
      this.showResponsive = false;
    } else {
      this.showResponsive = true;
    }
  }

  addToOrder(pizza) {
    this.pizzaShoppingService.addCartItem(pizza);
  }

  showResponsiveCart() {
    this.showResponsive = !this.showResponsive;
  }

  showResponsiveCartButton() {
    return window.innerWidth > 700;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
