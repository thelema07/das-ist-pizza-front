import { Component, OnInit, HostListener } from "@angular/core";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
  showPhoneLinks: boolean = true;
  @HostListener("window:resize", ["$event"])
  onResize(event) {
    if (event.target.innerWidth > 700) {
      this.showPhoneLinks = false;
    } else {
      this.showPhoneLinks = true;
    }
  }

  constructor() {}

  ngOnInit() {
    if (window.innerWidth > 700) {
      this.showPhoneLinks = false;
    } else {
      this.showPhoneLinks = true;
    }
  }
}
