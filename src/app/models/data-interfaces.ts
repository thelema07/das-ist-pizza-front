export interface CartItem {
  price: number;
  name: string;
  description?: string;
  ingredients?: string;
  uuid?: any;
  remove?: boolean;
}

export interface StateTree {
  store: CartItem[];
  cart: CartItem[];
  total: Totals;
  checkout: boolean;
}

export interface Totals {
  subTotal: number;
  delivery: number;
  grandTotal: number;
}
