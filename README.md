# README

Thelema Cuisine - Angular 8 Shopping Cart

### Specifications

- Quick summary

App made with testing purposes, using Angular 8 as frontend solution, using Rxjs for state management and data handling, using a single API Call, a customer can order, choose and navigate to order pizza, with 8 given options, the user can choose, add and remove any item he wants, and went he's ready, he can proceed to a checkout order.

- Version

  1.0.0

### Technology Stack

This app was created using

- Angular 8
- Bootstrap CSS
- Ngx Bootstrap
- Font Awesome Icons
- Google Fonts

### Contribution guidelines

- Download the repo
- Go to console and run "npm install"
- Go to console and run "ng serve"
- Go to the browser and open: http://localhost:4200/
- In order for this app to work properly, must be connected with its backend from the other repo.

### Owner

- Daniel Felipe Mesu
- danielrojo1927@gmail.com
